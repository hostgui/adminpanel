const fs = require('fs')
const path = require('path')
const gulp = require('gulp')
const buffer = require('vinyl-buffer')
const browserify = require('browserify')
const stylus = require('gulp-stylus')
const sourcemaps = require('gulp-sourcemaps')
const tap = require('gulp-tap')
const rename = require('gulp-rename')
const watch = require('gulp-watch')

const pages = fs.readdirSync(path.resolve(__dirname, './client/pages'))
const jsTasks = pages.map(page => `page-${page}.js`)
const cssTasks = pages.map(page => `page-${page}.css`)

pages.forEach(addPageTask)

gulp.task('css', gulp.parallel(...cssTasks))
gulp.task('js', gulp.parallel(...jsTasks))
gulp.task('default', gulp.parallel('css', 'js'))
gulp.task('watch', watchTask)

function addPageTask (page) {
    gulp.task(`page-${page}.js`, () => {
        const srcPath = path.resolve(__dirname, `./client/pages/${page}/${page}.script.js`)
        const buildPath = path.resolve(__dirname, `./dist/js`)

        if (!fs.existsSync(srcPath)) {
            return
        }

        return gulp.src(srcPath, { read: false })
            .pipe(buffer())
            .pipe(sourcemaps.init({ loadMaps: true }))
            .pipe(tap(file => {
                file.contents = browserify(file.path, { debug: true }).bundle()
            }))
            .pipe(sourcemaps.write('./'))
            .pipe(rename({ basename: `${page}`, extname: '.min.js' }))
            .pipe(gulp.dest(buildPath))
    })

    gulp.task(`page-${page}.css`, () => {
        const srcPath = path.resolve(__dirname, `./client/pages/${page}/${page}.style.styl`)
        const buildPath = path.resolve(__dirname, `./dist/css`)

        if (!fs.existsSync(srcPath)) {
            return
        }

        return gulp.src(srcPath)
            .pipe(sourcemaps.init({ loadMaps: true }))
            .pipe(stylus({
                'include css': true,
                include: [path.resolve(__dirname, './node_modules')]
            }))
            .pipe(sourcemaps.write('.'))
            .pipe(rename({ basename: `${page}`, extname: '.min.css' }))
            .pipe(gulp.dest(buildPath))
    })
}

function watchTask () {
    watch(path.resolve(__dirname, './client/**/*.js'), gulp.series('js'))
    watch(path.resolve(__dirname, './client/**/*.styl'), gulp.series('css'))
}
